# Description: Hands-on tutorial
# Book: Reliability-Based Design in Soil and Rock Engineering
# Example: Section 2.2 A simple hands-on Hasofer-Lind Method,
# Method: Monte-Carlo Simulation

import numpy as np
from typing import List
from scipy import stats
from pathlib import Path
import matplotlib.pyplot as plt
from matplotlib.ticker import PercentFormatter


def calc_Nq(phi: float) -> float:
    """Bearing capacity factor Nq"""
    phi = np.radians(phi)
    return np.exp(np.pi * np.tan(phi)) * np.tan(np.pi / 4 + phi / 2) ** 2


def calc_Nc(phi: float) -> float:
    """Bearing capacity factor Nc"""
    return (calc_Nq(phi) - 1) * 1 / np.tan(np.radians(phi))


def calc_Ng(phi: float) -> float:
    """Bearing capacity factor Ngamma"""
    return 2 * (calc_Nq(phi) + 1) * np.tan(np.radians(phi))


def calc_qu(
    c: np.ndarray, phi: np.ndarray, gamma: float, po: float, B: float
) -> np.ndarray:
    """Bearing capacity equation for strip footing with vertical load"""
    return c * calc_Nc(phi) + po * calc_Nq(phi) + 0.5 * B * gamma * calc_Ng(phi)


def gen_soil_params(
    c_mean: float,
    phi_mean: float,
    c_std: float,
    phi_std: float,
    corr: float,
    n_samples: float,
) -> np.ndarray:
    """Generate corrleated random samples"""
    means = [c_mean, phi_mean]
    stds = [c_std, phi_std]
    covs = [
        [stds[0] ** 2, stds[0] * stds[1] * corr],
        [stds[0] * stds[1] * corr, stds[1] ** 2],
    ]
    return np.random.multivariate_normal(means, covs, n_samples)


def simulation(cs: np.ndarray, phis: np.ndarray, gamma: float, po: float, B: float):
    """Run one simulation"""


def main():
    # paths
    path_root = Path(r"../")
    path_plt = path_root / "plt"
    filename_fig = "example_01_mc.png"
    ffig = path_plt.joinpath(filename_fig)

    # soil parameters
    # c intercept [kN/m2]
    c_mean = 20
    c_std = 5
    # phi angle [deg]
    phi_mean = 15
    phi_std = 2
    # correlation coeff
    corr = -0.5

    # unit weight [kN/m3]
    gamma = 20

    # foundation parameters
    # foundation depth [m]
    Df = 0.9
    # footing width [m]
    B = 1.2
    # vertical effective stress at foundation level [kN/m2]
    po = Df * gamma

    # footing load [kN/m]
    Qv = 200
    # footing stress [kN/m2]
    qload = Qv / B

    # calc bearing capacity
    Nq = calc_Nq(phi_mean)
    Nc = calc_Nc(phi_mean)
    Ng = calc_Ng(phi_mean)
    qu = calc_qu(c_mean, phi_mean, gamma, po, B)
    print(f"Overburden pressure, po: {po: 0.3f}")
    print(f"Bearing capacity factors: Nq: {Nq:0.3f}, Nc: {Nc:0.3f}, Ng: {Ng:0.3f}")
    print(f"Bearing capacity: {qu: 0.2f}")
    print(f"Loading: {qload: 0.2f}")

    # generate soil parameters
    # number of random variables
    n_samples = 100000
    means = [c_mean, phi_mean]
    stds = [c_std, phi_std]
    covs = [
        [stds[0] ** 2, stds[0] * stds[1] * corr],
        [stds[0] * stds[1] * corr, stds[1] ** 2],
    ]
    np.random.multivariate_normal(means, covs, n_samples)

    # number of trials
    ntrials = 200
    pfs: List[float] = []

    for _ in range(ntrials):
        soil_params = np.random.multivariate_normal(means, covs, n_samples)
        cs = soil_params[:, 0]
        phis = soil_params[:, 1]
        qu = calc_qu(cs, phis, gamma, po, B)
        pf = (qu < qload).sum() / n_samples
        # beta = -stats.norm.ppf(pf)
        pfs.append(pf)
        # print(f"pf: {pf*100:0.4f}, beta: {beta:0.4f}")
    pfs = np.asarray(pfs)
    pfs_mean = pfs.mean()
    pfs_std = pfs.std()
    beta_mean = -stats.norm.ppf(pfs_mean)
    print(f"Probability of failure (%): {pfs_mean * 100:0.3f}")
    print(f"Reliability Index, beta: {beta_mean:0.3f}")

    # probability of failure from FORM
    pf_form = 0.00054

    # figure size
    figscale = 2.0
    figw = 3.487
    figh = figw / 1.618
    figsz = (figscale * figw, figscale * figh)

    # figure
    fig, ax = plt.subplots(figsize=figsz)
    # plot histogram
    counts, bins, patches = plt.hist(
        pfs, bins=20, facecolor="g", edgecolor="gray", alpha=0.75
    )
    # draw a vertical line at FORM pf
    ax.axvline(pf_form, ls="--", color="k", lw=2)
    ax.text(
        pf_form - 0.00002,
        10,
        fr"p$_f$ FORM = {pf_form*100: 0.3f}$\%$",
        rotation="vertical",
    )

    # gird
    ax.grid(visible=True, which="major", axis="y", ls=":", alpha=0.5)
    ax.grid(visible=True, which="minor", axis="x", ls=":", alpha=0.5)

    # change x-axis labels to %
    ax.xaxis.set_major_formatter(PercentFormatter(1.0, decimals=3))

    # axis labels
    ax.set_xlabel(r"Probability of failure, P$_f$, [$\%$]")
    ax.set_ylabel(r"Frequency")

    # save figure
    plt.savefig(ffig, format="png", bbox_inches="tight", dpi=600)


if __name__ == "__main__":
    main()
