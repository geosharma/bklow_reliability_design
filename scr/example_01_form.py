# Description: Hands-on tutorial
# Book: Reliability-Based Design in Soil and Rock Engineering
# Example: Section 2.2 A simple hands-on Hasofer-Lind Method

import numpy as np
import numpy.typing as npt
from typing import List
from scipy import stats
import scipy.optimize as optimize


def calc_Nq(phi: float) -> float:
    """Bearing capacity factor Nq"""
    phi = np.radians(phi)
    return np.exp(np.pi * np.tan(phi)) * np.tan(np.pi / 4 + phi / 2) ** 2


def calc_Nc(phi: float) -> float:
    """Bearing capacity factor Nc"""
    return (calc_Nq(phi) - 1) * 1 / np.tan(np.radians(phi))


def calc_Ng(phi: float) -> float:
    """Bearing capacity factor Ngamma"""
    return 2 * (calc_Nq(phi) + 1) * np.tan(np.radians(phi))


def calc_qu(c: float, phi: float, gamma: float, po: float, B: float) -> float:
    """Bearing capacity equation for strip footing with vertical load"""
    return c * calc_Nc(phi) + po * calc_Nq(phi) + 0.5 * B * gamma * calc_Ng(phi)


def con(
    n: np.ndarray,
    x_mean: np.ndarray,
    x_std: np.ndarray,
    gamma: float,
    po: float,
    B: float,
    qv: float,
) -> float:
    """Define constraints for minimization"""
    x: np.ndarray
    n = np.array(n).reshape(2, 1)
    x = n * x_std + x_mean
    return calc_qu(x[0][0], x[1][0], gamma, po, B) - qv


def beta(n: np.ndarray, cor: np.ndarray) -> float:
    """Reliability Index"""
    return np.sqrt(n.T @ np.linalg.inv(cor) @ n)


def main():
    # soil parameters
    # c intercept [kN/m2]
    c_mean = 20
    c_std = 5
    # phi angle [deg]
    phi_mean = 15
    phi_std = 2
    # unit weight [kN/m3]
    gamma = 20

    # foundation parameters
    # foundation depth [m]
    Df = 0.9
    # footing width [m]
    B = 1.2
    # vertical effective stress at foundation level [kN/m2]
    po = Df * gamma

    # footing load [kN/m]
    Qv = 200
    # footing stress [kN/m2]
    qv = Qv / B

    # calc bearing capacity
    Nq = calc_Nq(phi_mean)
    Nc = calc_Nc(phi_mean)
    Ng = calc_Ng(phi_mean)
    qu = calc_qu(c_mean, phi_mean, gamma, po, B)
    print(f"Overburden pressure, po: {po: 0.3f}")
    print(f"Bearing capacity factors: Nq: {Nq:0.3f}, Nc: {Nc:0.3f}, Ng: {Ng:0.3f}")
    print(f"Bearing capacity: {qu: 0.2f}")
    print(f"Loading: {qv: 0.2f}")

    # initial n values
    n = np.array([0, 0]).reshape(2, 1)
    x_mean = np.array([c_mean, phi_mean]).reshape(2, 1)
    x_std = np.array([c_std, phi_std]).reshape(2, 1)
    cor = np.array([1, -0.5, -0.5, 1]).reshape(2, 2)
    # create constraints
    # ref: https://stackoverflow.com/questions/20075714/scipy-minimize-with-constraints
    cons = {"type": "eq", "fun": con, "args": (x_mean, x_std, gamma, po, B, qv)}
    result = optimize.minimize(beta, n, args=(cor,), constraints=cons)
    n = result["x"].reshape(2, 1)
    # reliability index
    beta_comp = result["fun"]
    # probability of failure
    pf = 1 - stats.norm.cdf(beta_comp)
    # betaf = np.sqrt(n.T @ np.linalg.inv(cor) @ n)
    # values of c and phi after minimization
    x = n * x_std + x_mean

    print(f"cor: {cor}")
    print(f"n: {n}")
    print(f"x: {x}")
    print(f"Reliability Index, beta: {beta_comp:0.3f}")
    print(f"Probability of failure (%): {pf * 100:0.3f}")


if __name__ == "__main__":
    main()
