# Description: Hands-on tutorial
# ref: https://stackoverflow.com/questions/18683821/generating-random-correlated-x-and-y-points-using-numpy
# or https://scipy-cookbook.readthedocs.io/items/CorrelatedRandomSamples.html
# Example: Generate correlated random variables based on example_01 parameters

import numpy as np
from pathlib import Path
import matplotlib.pyplot as plt

# Updated parameters for this plot
plt.style.use("../plt/figstylefile.mplstyle")


def main():
    # paths
    path_root = Path(r"../")
    path_plt = path_root / "plt"
    filename_fig = "example_00_correlated_random_variables.png"
    ffig = path_plt.joinpath(filename_fig)

    # soil parameters
    # c intercept [kN/m2]
    c_mean = 20
    c_std = 5
    # phi angle [deg]
    phi_mean = 15
    phi_std = 2
    corr = -0.5

    means = [c_mean, phi_mean]
    stds = [c_std, phi_std]
    covs = [
        [stds[0] ** 2, stds[0] * stds[1] * corr],
        [stds[0] * stds[1] * corr, stds[1] ** 2],
    ]

    # generate uncorrelated normal random variables
    n_samples = 1000
    cor_randvars = np.random.multivariate_normal(means, covs, n_samples)
    cs = cor_randvars[:, 0]
    phis = cor_randvars[:, 1]

    # figure size
    figscale = 2.0
    figw = 3.487
    figh = figw / 1.618
    figsz = (figscale * figw, figscale * figh)

    # figure
    fig, ax = plt.subplots(figsize=figsz)
    ax.scatter(phis, cs)

    # set y-axis scale
    # ax.set_ylim(0, 100)
    ax.grid(visible=True, which="major", axis="y", ls=":", alpha=0.5)
    ax.grid(visible=True, which="minor", axis="x", ls=":", alpha=0.5)
    # axis labels
    ax.set_xlabel(r"Friction angle, $\phi^{'}$ [$^o$]")
    ax.set_ylabel(r"Cohesion, c$^{'}$ [kN/m$^2$]")
    plt.savefig(ffig, format="png", bbox_inches="tight", dpi=600)


if __name__ == "__main__":
    main()
