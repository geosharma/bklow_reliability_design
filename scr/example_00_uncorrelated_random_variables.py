# Description: Hands-on tutorial
# Example: Generate random variables based on example_01 parameters


import numpy as np
from pathlib import Path
import matplotlib.pyplot as plt

# Updated parameters for this plot
plt.style.use("../plt/figstylefile.mplstyle")


def main():
    # paths
    path_root = Path(r"../")
    path_plt = path_root / "plt"
    filename_fig = "example_00_uncorrelated_random_variables.png"
    ffig = path_plt.joinpath(filename_fig)

    # soil parameters
    # c intercept [kN/m2]
    c_mean = 20
    c_std = 5
    # phi angle [deg]
    phi_mean = 15
    phi_std = 2

    # generate uncorrelated normal random variables
    n_samples = 10000
    cs = np.random.normal(c_mean, c_std, n_samples)
    phis = np.random.normal(phi_mean, phi_std, n_samples)

    # figure size
    figscale = 2.0
    figw = 3.487
    figh = figw / 1.618
    figsz = (figscale * figw, figscale * figh)

    # figure
    fig, ax = plt.subplots(figsize=figsz)
    ax.scatter(phis, cs)

    # set y-axis scale
    # ax.set_ylim(0, 100)
    ax.grid(visible=True, which="major", axis="y", ls=":", alpha=0.5)
    ax.grid(visible=True, which="minor", axis="x", ls=":", alpha=0.5)
    # axis labels
    ax.set_xlabel(r"Friction angle, $\phi^{'}$ [$^o$]")
    ax.set_ylabel(r"Cohesion, c$^{'}$ [kN/m$^2$]")
    plt.savefig(ffig, format="png", bbox_inches="tight", dpi=600)


if __name__ == "__main__":
    main()
