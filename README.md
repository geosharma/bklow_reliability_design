# Reliability-Based Design in Soil and Rock Engineering Enhancing Partial Factor Design Approaches
Author: Bak Kong Low

This repository consists of some examples of the book that are implemented in Python. I will try to implement both the first order reliability method (FORM) as given in the book and equivalent Monte-Carlo simulation, as my knowledge permits, which, as it will be evident, is not good.

## Requirements
I did not create an environment specially for this project, so my conda environment has many packages. These are some of the packages that are needed. I keep updating my environment thus the versions change and are approximately the latest version available in conda.

- Python
- numpy
- scipy
- matplotlib

## Examples Completed
This is a work in progress. Thus far I have completed these examples with :heavy_check_mark:.


| Filename          | Page | Section |FORM               | Monte-Carlo             | Remarks |
|:------------------|-----:|---------|:-----------------:|:-----------------------:|---------|
|example_00_uncorrelated_random_variables.py | -   | - | |                         |         |
|example_00_correlated_random_variables.py | -   | -  |  |                         |         |
|example_01_form.py |   10 | 2.2     |:heavy_check_mark: |                         |         |
|example_01_mc.py   |   10 | 2.2     |:heavy_check_mark: |                         |         |